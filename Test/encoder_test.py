import unittest

from morse_code import encoder

import requests


class EncoderTest(unittest.TestCase):
    """
    Test encoder method of morse code module
    """

    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)

    def test_letter(self):
        message = 'E'
        response = requests.get('https://api.funtranslations.com/translate/morse.json?text={}'.format(message))
        self.assertTrue(encoder(message) == response.json()['contents']['translated'])

    def test_space(self):
        message = ' '
        response = requests.get('https://api.funtranslations.com/translate/morse.json?text={}'.format(message))
        self.assertTrue(encoder(message) == response.json()['contents']['translated'])

    def test_word(self):
        message = 'BLU'
        response = requests.get('https://api.funtranslations.com/translate/morse.json?text={}'.format(message))
        self.assertTrue(encoder(message) == response.json()['contents']['translated'])

    def test_two_word(self):
        message = 'BLUBANK'
        response = requests.get('https://api.funtranslations.com/translate/morse.json?text={}'.format(message))
        self.assertTrue(encoder(message) == response.json()['contents']['translated'])

    def test_sentence(self):
        message = 'BANKBUTLOVELY'
        response = requests.get('https://api.funtranslations.com/translate/morse.json?text={}'.format(message))
        self.assertTrue(encoder(message) == response.json()['contents']['translated'])


if __name__ == '__main__':
    unittest.main()
