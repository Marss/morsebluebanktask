from flask import Flask, jsonify, request

from morse_code import encoder

app = Flask(__name__)


@app.route("/encrypt/<string:message>", methods=['GET'])
def encoder(message):
    if request.method == 'GET':
        return jsonify({'coded': encoder(message)})


if __name__ == '__main__':
    app.run()
